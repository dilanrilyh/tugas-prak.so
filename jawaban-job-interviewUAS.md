# 2. Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi.

Untuk membuat Docker Image menggunakan Dockerfile bisa sebagai berikut:


- Base Image: Pertama, Dockerfile dimulai dengan  FROM debian:bullseye-slim. 


- Install Dependensi: Selanjutnya,RUN apt-get update && apt-get install -y git neovim netcat python3-pip  untuk mengupdate paket dan menginstal dependensi yang diperlukan dalam kontainer. Dependensi yang diinstal termasuk git, neovim, netcat, dan python3-pip.


- Copy File Aplikasi: Pernyataan COPY hadits-d.py /home/hadits-digital/hadits-d.py dan COPY requirements.txt /home/hadits-digital/requirements.txt digunakan untuk menyalin file aplikasi meditasi.py dan file requirements.txt ke dalam kontainer. File aplikasi dan file dependensi ini akan digunakan dalam langkah-langkah selanjutnya.


- Install Dependensi Python: RUN pip3 install --no-cache-dir -r /home/meditasi.py/requirements.txt digunakan untuk menginstal dependensi Python yang diperlukan dalam aplikasi. File requirements.txt berisi daftar dependensi Python yang dibutuhkan oleh aplikasi.


- Set Working Directory: Pernyataan WORKDIR /home/hadits-digital menetapkan direktori kerja di dalam kontainer sebagai /home/warung.py. Ini adalah direktori di mana file-file aplikasi dan dependensi akan berada.


- Expose Port: Pernyataan EXPOSE 25040 digunakan untuk mengekspos port 25040 di dalam kontainer. Port ini akan digunakan untuk mengakses aplikasi Thrifting Pakaian yang berjalan dalam kontainer.


- Start Web Service: Pernyataan CMD ["streamlit", "run", "--server.port=25040", "meditasi.py"] digunakan untuk menjalankan perintah saat kontainer dimulai. Pada kasus ini, perintah streamlit run --server.port=25040 meditasi.py akan dijalankan untuk menjalankan aplikasi Streamlit.

Setelah Dockerfile selesai, Anda dapat menggunakan perintah docker build untuk membangun Docker Image dari Dockerfile tersebut. Contoh perintahnya adalah:

# 3. Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container.


- Langkah-langkah:

1. Buat direktori baru untuk proyek dan masuk ke dalamnya.

2. Buat file Dockerfile untuk mendefinisikan Docker image yang akan digunakan.

3. Buat file requirements.txt untuk mengelola dependensi Python yang diperlukan.

4. Buat file meditasi.py yang akan berisi kode aplikasi web.


5. Konfigurasi Docker:

Membuat file Dockerfile:

```
FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web service files
COPY meditasi.py /home/program-meditasi/meditasi.py
COPY requirements.txt /home/program-meditasi//requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/program-meditasi//requirements.txt

#set working directory
WORKDIR /home/program-meditasi/

#expose port
EXPOSE 25040

#start web service
CMD ["streamlit", "run", "--server.port=25040", "meditasi.py"]
```
4.  Membuat file meditasi.py :

```
import streamlit as st
import requests

# Fungsi untuk mengirim permintaan ke API dan mendapatkan respons
def kirim_permintaan(url):
    try:
        response = requests.get(url)
        return response.json()
    except requests.exceptions.RequestException as e:
        st.error("Terjadi kesalahan saat mengirim permintaan ke server.")
        st.stop()

# Fungsi untuk menampilkan menu utama
def tampilkan_menu_utama():
    st.title("KONSELING MEDITASI")
    pilihan = st.sidebar.selectbox("Menu", ["Mulai Meditasi", "Informasi Meditasi"])

    if pilihan == "Mulai Meditasi":
        tampilkan_meditasi()
    elif pilihan == "Informasi Meditasi":
        tampilkan_informasi()

# Fungsi untuk menampilkan meditasi
def tampilkan_meditasi():
    st.title("MULAI MEDITASI")
    st.write("Silakan duduk dengan nyaman dan tutup mata Anda.")
    st.write("Kendalikan pernapasan Anda dan fokuskan pikiran pada pernapasan yang dalam dan lambat.")
    st.write("Biarkan pikiran-pikiran yang muncul lewat tanpa menghakiminya.")
    st.write("Teruskan meditasi ini selama 10 menit.")

# Fungsi untuk menampilkan informasi meditasi
def tampilkan_informasi():
    st.title("INFORMASI MEDITASI")
    st.write("Meditasi adalah praktik yang digunakan untuk mengatasi stres, meningkatkan konsentrasi, dan mencapai ketenangan pikiran.")
    st.write("Cara melakukan meditasi:")
    st.write("- Duduk dengan nyaman di tempat yang tenang.")
    st.write("- Tutup mata dan fokuskan perhatian pada pernapasan.")
    st.write("- Biarkan pikiran-pikiran yang muncul lewat tanpa menghakiminya.")
    st.write("- Teruskan meditasi selama waktu yang Anda tentukan.")

# URL API untuk meditasi
api_url = "https://api.meditation.com/meditation"

# Program Utama
if __name__ == "__main__":
    tampilkan_menu_utama()
```

# 4. Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing.

- Buat direktori baru untuk proyek dan masuk ke dalamnya.

- Buat file docker-compose.yml untuk mendefinisikan layanan yang akan dijalankan.

Konfigurasi Docker Compose:

- Membuat file docker-compose.yml:

```
version: "3"
services:
  program-meditasi:
    image: dilanrilyh/program-meditasi:1.0
    ports:
    - "25040:25040"
    networks:
    - niat-yang-suci
    volumes:
    - /home/praktikumc/1217050040/program-meditasi:/program-meditasi

  networks:
    niat-yang-suci:
      external: true
```


# 5. Mampu menjelaskan project yang dibuat dalam bentuk file README.md

1. Deskripsi project
Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan beberapa solusi dalam melakukan meditasi dalam menangani suatu masalah.

- Import Library


`import streamlit as st`

Kode di atas mengimpor  library yang digunakan dalam program ini, yaitu streamlit.streamlit adalah library yang digunakan untuk membuat antarmuka web.

- Fungsi untuk menampilkan menu utama

```
def tampilkan_menu_utama():
    st.title("KONSELING MEDITASI")
    pilihan = st.sidebar.selectbox("Menu", ["Mulai Meditasi", "Informasi Meditasi"])

    if pilihan == "Mulai Meditasi":
        tampilkan_meditasi()
    elif pilihan == "Informasi Meditasi":
        tampilkan_informasi()
```
Fungsi ini digunakan untuk menampilkan daftar meditasi  ke antarmuka web (Interface) menggunakan streamlit. Setiap informasi meditasi dimenu utama.


- Fungsi untuk menampilkan meditasi

```
def tampilkan_meditasi():
    st.title("MULAI MEDITASI")
    st.write("Silakan duduk dengan nyaman dan tutup mata Anda.")
    st.write("Kendalikan pernapasan Anda dan fokuskan pikiran pada pernapasan yang dalam dan lambat.")
    st.write("Biarkan pikiran-pikiran yang muncul lewat tanpa menghakiminya.")
    st.write("Teruskan meditasi ini selama 10 menit.")
```
Fungsi ini digunakan untuk menampilkan daftar meditasi  ke antarmuka web (Interface) menggunakan streamlit. Setiap informasi meditasi ditampilkan.

- Fungsi untuk menampilkan informasi meditasi

```
def tampilkan_informasi():
    st.title("INFORMASI MEDITASI")
    st.write("Meditasi adalah praktik yang digunakan untuk mengatasi stres, meningkatkan konsentrasi, dan mencapai ketenangan pikiran.")
    st.write("Cara melakukan meditasi:")
    st.write("- Duduk dengan nyaman di tempat yang tenang.")
    st.write("- Tutup mata dan fokuskan perhatian pada pernapasan.")
    st.write("- Biarkan pikiran-pikiran yang muncul lewat tanpa menghakiminya.")
    st.write("- Teruskan meditasi selama waktu yang Anda tentukan.")
```
Fungsi ini digunakan untuk menampilkan daftar meditasi  ke antarmuka web (Interface) menggunakan streamlit. Setiap informasi meditasi secara lengkap.

- URL API untuk meditasi
`api_url = "https://api.meditation.com/meditation"`

- Program Utama
```
if __name__ == "__main__":
    tampilkan_menu_utama()
```



- penjelasan bagaimana sistem operasi digunakan dalam proses containerization

Dalam proses containerization, sistem operasi digunakan untuk:

Isolasi sumber daya antara container.
Pengaturan jaringan untuk container.
Manajemen sumber daya seperti CPU dan memori.
Pemutakhiran dan perbaikan sistem operasi.
Interaksi dengan container runtime seperti Docker atau Kubernetes.

Sistem operasi yang umum digunakan dalam containerization adalah Linux dengan kernel yang mendukung fitur-fitur container. Ada juga sistem operasi khusus untuk kontainer seperti CoreOS atau RancherOS.

- Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi

Containerization mempermudah pengembangan aplikasi dengan cara-cara berikut:

Lingkungan yang konsisten di seluruh siklus pengembangan.
Portabilitas aplikasi di berbagai lingkungan.
Skalabilitas yang mudah untuk menangani permintaan yang meningkat.
Isolasi dan keamanan aplikasi yang lebih baik.
Proses pengiriman yang cepat dan efisien.

Dengan menggunakan container, pengembang dapat menghindari masalah kompatibilitas, mempercepat proses pengiriman, meningkatkan skalabilitas, dan memastikan lingkungan yang konsisten.

- Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi

DevOps adalah pendekatan dalam pengembangan perangkat lunak yang menggabungkan praktik-praktik pengembangan (Development) dan operasional (Operations) secara terintegrasi. DevOps bertujuan untuk menciptakan alur kerja yang efisien, berkelanjutan, dan berkolaborasi antara tim pengembangan dan tim operasional.
DevOps membantu pengembangan aplikasi dengan beberapa cara:


Kolaborasi Tim yang Kuat: DevOps mendorong kolaborasi yang erat antara tim pengembangan dan tim operasional. Ini memungkinkan komunikasi yang lebih baik, pemahaman yang lebih mendalam tentang kebutuhan bisnis, dan koordinasi yang efisien dalam menghadapi tantangan dan perubahan dalam pengembangan aplikasi. Kolaborasi yang kuat memungkinkan pengembang untuk memahami persyaratan operasional dan memastikan bahwa aplikasi dapat diimplementasikan dan dikelola dengan lancar.


Automasi Proses dan Pengujian: DevOps menganjurkan otomatisasi dalam berbagai aspek pengembangan aplikasi. Automasi dapat diterapkan dalam tahap pembangunan, pengujian, pengiriman, dan pemeliharaan aplikasi. Dengan otomatisasi, tugas-tugas berulang dapat diatasi dengan cepat, memungkinkan pengembang untuk fokus pada inovasi dan pengembangan fitur baru. Automasi juga membantu memastikan kualitas aplikasi melalui pengujian otomatis yang terintegrasi.


Penerapan Continuous Integration/Continuous Deployment (CI/CD): DevOps mendorong penerapan CI/CD, yaitu pendekatan di mana perubahan kode secara terus-menerus diuji, diintegrasikan, dan diimplementasikan ke lingkungan produksi secara otomatis. CI/CD memungkinkan pengembang untuk dengan cepat menerapkan perubahan, mengurangi waktu penyebaran, dan mengurangi risiko kesalahan. Dengan adopsi CI/CD, pengembangan aplikasi menjadi lebih responsif dan dapat memenuhi kebutuhan pasar yang berubah dengan cepat.


Monitoring dan Tindak Lanjut: DevOps menekankan pentingnya pemantauan aplikasi secara terus-menerus dan respons cepat terhadap masalah yang muncul. Dengan menggunakan alat pemantauan dan logging yang tepat, tim DevOps dapat mendapatkan wawasan tentang kinerja aplikasi dan melakukan tindakan yang diperlukan untuk mengatasi masalah sebelum berdampak pada pengguna. Dengan demikian, DevOps membantu memastikan ketersediaan dan keandalan aplikasi secara terus-menerus.


Perubahan Budaya dan Mindset: DevOps melibatkan perubahan budaya dan mindset dalam pengembangan aplikasi. Ini mencakup kolaborasi tim lintas fungsi, penerapan tanggung jawab bersama, dan adopsi siklus umpan balik yang cepat untuk terus meningkatkan proses dan produk. DevOps mendorong budaya eksperimen, di mana pengembang dapat mencoba hal-hal baru dan memperbaiki proses secara berkelanjutan.


Secara keseluruhan, DevOps membantu pengembangan aplikasi dengan mengintegrasikan tim dan alur kerja, menerapkan otomasi, mengadopsi CI/CD, meningkatkan monitoring dan tindak lanjut, serta mengubah budaya dan mindset. Dengan pendekatan ini, pengembang dapat mengurangi waktu siklus pengembangan, meningkatkan kualitas, dan memberikan aplikasi yang lebih responsif dan andal kepada pengguna.

- Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

1. Gojek, perusahaan teknologi terkemuka dalam industri layanan on-demand, telah menerapkan DevOps sebagai pendekatan yang fundamental dalam pengembangan aplikasi mereka. Berikut ini adalah contoh implementasi DevOps di Gojek:


2. Automasi Proses: Gojek menggunakan alat otomasi seperti Ansible, Terraform, dan Kubernetes untuk mengotomatiskan proses pengembangan dan pengiriman aplikasi. Dengan otomasi ini, Gojek dapat dengan cepat menyediakan infrastruktur, mengatur konfigurasi, dan melakukan pengujian otomatis. Proses pengembangan dan penyebaran menjadi lebih efisien dan konsisten, memungkinkan tim untuk lebih fokus pada inovasi dan pengembangan fitur.


3. Continuous Integration/Continuous Deployment (CI/CD): Gojek menerapkan CI/CD sebagai bagian dari alur kerja DevOps. Mereka menggunakan alat seperti Jenkins dan GitLab CI/CD untuk mengotomatiskan pengujian dan penyebaran aplikasi. Setiap kali ada perubahan kode yang diajukan, sistem CI/CD akan memicu serangkaian pengujian otomatis untuk memastikan kualitas kode. Jika pengujian berhasil, perubahan kode akan secara otomatis diterapkan ke lingkungan produksi. Pendekatan ini memungkinkan Gojek untuk menerapkan perubahan dengan cepat dan responsif, sehingga mengurangi waktu penyebaran dan meningkatkan keandalan aplikasi.


4. Kolaborasi Tim: Gojek mendorong kolaborasi yang erat antara tim pengembangan, tim operasional, dan tim keamanan. Mereka memastikan komunikasi yang terbuka dan saling mendukung antara tim-tim ini. Kolaborasi yang kuat memungkinkan pengembang untuk lebih memahami kebutuhan operasional, sementara tim operasional dapat memberikan masukan yang berharga dalam perancangan dan pengiriman aplikasi. Kebersamaan tim juga membantu mempercepat perbaikan masalah dan mengoptimalkan alur kerja.


5. Monitoring dan Pengelolaan Keandalan: Gojek sangat memperhatikan monitoring aplikasi dan pengelolaan keandalan layanan mereka. Mereka menggunakan berbagai alat seperti Grafana, Prometheus, dan ELK Stack untuk memantau kinerja aplikasi dan mendeteksi masalah dengan cepat. Tim operasional dapat merespons dengan cepat terhadap gangguan layanan, memperbaiki masalah, dan memastikan tingkat keandalan yang tinggi. Monitoring secara berkelanjutan juga membantu Gojek dalam meningkatkan kinerja aplikasi dan merespons perubahan yang diperlukan dengan cepat.


Adopsi Teknologi Cloud Native: Gojek telah mengadopsi teknologi cloud native seperti Kubernetes dan Docker untuk mengelola aplikasi mereka secara efisien. Dengan menggunakan teknologi ini, Gojek dapat mengelola skala aplikasi yang besar, memastikan elastisitas, dan dengan cepat melakukan penyebaran aplikasi ke berbagai lingkungan. Pendekatan cloud native juga memungkinkan Gojek untuk memanfaatkan fleksibilitas dan skalabilitas yang ditawarkan oleh platform cloud seperti AWS, GCP, dan Azure.


Melalui implementasi DevOps yang kokoh, Gojek telah berhasil meningkatkan efisiensi pengembangan aplikasi mereka, mempercepat waktu penyebaran, meningkatkan keandalan layanan, dan menghadapi persaingan bisnis yang ketat. Penerapan DevOps membantu Gojek untuk tetap responsif terhadap perubahan pasar dan memberikan pengalaman pengguna yang unggul dalam lingkungan teknologi yang terus berkembang.


# 6. Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube.

# 7. Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing.
![Dokumentasi interview uts](Dokumentasi interview uts/dockerhub.png)

https://hub.docker.com/repository/docker/dilanrilyh/program-meditasi/general
