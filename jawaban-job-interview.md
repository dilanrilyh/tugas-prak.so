# No.1 
Monitoring Resource dari komputer
- RAM untuk memonitoring RAM pada komputer  kita bisa menggunakan command free untuk melihat total, terpakai, dan memori yang tersisa.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot_2023-05-13_190329.png)
ketika hanya mengetikan command free, yang akan tampil adalah penggunaan RAM berdasarkan satuan kilobyte. bisa diubah tampilannya ke megabyte dengan mengetikan command free --mega atau free --giga.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__5_.png)
- CPU untuk melihat penggunaan CPU pada komputer, kita bisa menggunakan command ps atau top.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__28_.png)
- Hardisk
untuk melihat penggunaan Hardisk pada komputer, kita bisa menggunakan command df.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__29_.png)

# No.2
Manajemen Program
-Mampu memonitor program yang sedang berjalan kita dapat melihatnya menggunakan command ps atau top.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__6_.png)
-Mampu menghentikan program yang sedang berjalan kita perlu menggunakan command kill.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__7_.png)
-Otomasi perintah dengaan shell script kita dapat membuat file sh dengan command touch <nama.file>.sh
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__8_.png)
untuk membuka file sh tersebut kita dapat menggunakan command vim <nama.file>.sh
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__9_.png)
berikut merupakan tampilan file sh yang berisikan $1 (parameter 1), $2 (parameter 2), dan $3 (parameter 3)
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__30_.png)
kita dapat menggunakan command cat <nama.file>.sh untuk menampilkan file sh tadi, kemudian kita dapat menggunakan command ./namafile.sh lalu "parameter 1" "parameter 2" "parameter" kemudian akan tampil seperti dibawah ini:
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__31_.png)
-Penjadwalan eksekusi program dengan cron untuk menjadwalkan eksekusi program kita bisa menggunakan command crontab -e
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__12_.png)
kemudian akan muncul tampilan seperti ini
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/crontab.png)
disini file kucing.log sudah terdaftar list
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot_2023-05-26_062223.png)

# No.3
Manajemen Network
-Mengakses sistem operasi pada jaringan menggunakan SSH kita bisa login manual pada ssh dan mengetikan nama server juga passwordnya Bisa di lihat pada gambar di bawah ini:
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__14_.png)
-Mampu memonitor program yang menggunakan network kita bisa menggunakan command netstat -tulpn (untuk mengeluarkan beberapa program yang menggunakan network) dan netstat -tulpn|grep LISTEN (untuk mengeluarkan beberapa program yang menggunakan network pada state LISTEN) 
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__15_.png)
-Mampu mengoperasikan HTTP client kita bisa dengan menggunakan command curl http
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__16_.png)
selanjutnya untuk mendownload gambar dan sebagainya bisa menggunakan wget contohnya pada instagram seperti dibawah ini:
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__17_.png)

# No.4
Manajemen File dan Folder
-Navigasi itu terdapat command pwd yang digunakan untuk mengetahui kita sedang berada di directori mana mana, command cd digunakan jika jika ingin pindah directori, command ls digunakan untuk mengetahui list yang ada didalam directori.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__18_.png)
-CRUD File dan Folder
kita dapat menggunakan command mkdir untuk membuat directori baru, command touch untuk membuat folder baru, command mv untuk membuat file baru, rm untuk menghapus file, rmdir untuk menghapus directori.
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__19_.png)
untuk otomasi kita bisa gunakan shell script langkah-langkahnya sama seperti otomasi perintah pada soal no 2, berikut dokumentasinya:
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__21_.png)
-Manajemen ownership dan akses file dan folder kita bisa melihat nya terlebih dahulu menggunakan command ls -l
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__22_.png)
-Pencarian file dan folder dapat menggunakan command find
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot_2023-05-13_205217.png)
-Kompresi data dapat menggunakan command tar -cf namafile.tar namafile
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot_2023-05-13_205746.png)

# No.5
Mampu membuat program berbasis CLI menggunakan shell script
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__25_.png)
berikut adalah tampilannya
![Dokumentasi Interview uts](https://gitlab.com/dilanrilyh/tugas-prak.so/-/raw/main/Dokumentasi%20interview%20uts/Screenshot__31_.png)

# No.6 
Mendemonstrasikan program CLI yang dibuat kepada publik dalam bentuk Youtube https://www.youtube.com/watch?v=Z63SWYwMjsQ
